# Sets :geoname_id as index
class AddIndexToBlocks < ActiveRecord::Migration[5.1]
  def change
    add_index :blocks, :geoname_id
  end
end
