module Api
  module V1
    # Block controller
    class BlockController < ApplicationController
      include FindBlock
      # Find the block by ID
      # If a block is found, render JSON through the serializer
      # else render error
      def show
        @block = Block.find(params[:id])
        if @block
          render json: @block, serializer: BlockSerializer, root: 'ip_block'
        else
          @error = Error.new(text: '404 Not Found', status: 404,
                             url: request.url, method: request.method)
          render json: @error.serializer
        end
      end

      # The block of this method is fetched the concern
      # If found it is serialized and rendered; otherwise
      # an error is returned
      def get_block
        if @block
          render json: @block, serializer: BlockSerializer,
                 root: 'ip_block'
        else
          @error = Error.new(text: '404 Not found', status: 404,
                             url: request.url, method: request.method)
          render json: @error
        end
      end
    end
  end
end
