module Api
  module V1
    # Pins controller
    class PinsController < ApplicationController
      include EntryFiltering
      # Pins fetched by location
      # if no pins are provided send an error
      def index
        if @pins
          render json: @pins, each_serializer: PinSerializer,
                 root: 'pins'
        else
          @error = Error.new(text: '404 Not found', status: 404,
                             url: request.url, method: request.method)
          render json: @error.serializer
        end
      end

      # Find the pin by ID and return it serialized
      # if no pin is found we return an error
      def show
        @pin = Pin.find(params[:id])
        if @pin
          render json: @pin, serializer: PinSerializer, root: pin
        else
          @error = Error.new(text: '404 Not found', method: request.method,
                             status: 404, url: request.url)
        end
      end
    end
  end
end
