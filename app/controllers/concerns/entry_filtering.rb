module EntryFiltering
  extend ActiveSupport::Concern

  # Perform only entry action for index method
  included do
    before_action :entry_action , only: index
  end

  # point is a string containing geographical coordinates
  # in the form of {longitude},{latitude}
  def pins_by_point(point)
    point = point.split(',')
    @pins = Pin.find_near(point[0].to_f,point[1].to_f)
  end

  # If no coordinates are sent, try and locate the IP
  # and retrieve pins close to the IP location
  def pins_by_ip(ip)
    block = Block.where(network_start_ip: "::ffff:#{ip.rpartition('.')[0]}.0").first
    if block
      Pin.find_near(block.longitude.to_f, block.latitude.to_f)
    end
  end

  # Fetch pins by IP if no point param is provided
  def entry_action
    if params[:point]
      @pins = pins_by_point(params[:point])
    else
      @pins = pins_by_ip(request.remote_ip)
    end
  end
end