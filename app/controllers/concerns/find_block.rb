module FindBlock
  extend ActiveSupport::Concern
  # This snippet of code is executed when the concern is included
  # but only for the get_block action
  included do
    before_action :find_block, only: :get_block
  end

  # Set the IP address of the request. If the ip_address
  # param is present; otherwise use the request remote_ip
  def set_ip
    params[:ip_address] ?  params[:ip_address] : request.remote_ip
  end

  # Find the IP block corresponding to the IP address
  # Cosmetic work on IP string to remove last part of
  # the address (last octect) and replace it with 0
  def find_block
    ip = set_ip
    @block = Block
             .where(network_start_ip: "::ffff:#{ip.rpartition('.')[0]}.0")
             .first
  end
end