# The geo_factory function sets the actual
# lattitude-longitude point on the database column by using
# the values of lattitude and longitude
module GeoFactory
  extend ActiveSupport::Concern
  def geo_factory
    self.lonlat = self.class.rgeo_factory(longitude, latitude)
  end
  # Class methods definition
  module ClassMethods
    def rgeo_factory(longitude, latitude)
      RGeo::Geographic.spherical_factory
                      .point(longitude, latitude)
    end

    # find_near function searches records in a give radius
    def self.find_near(lon, lat, radius)
      factory = RGeo::Geographic.spherical_factory
      sw = factory.point(lon + radius, lat + radius)
      ne = factory.point(lon - radius, lat - radius)
      window = RGeo::Cartesian::BoundingBox
               .create_from_points(sw, ne)
               .to_geometry
      where('lonlat && :window', window)
    end
  end
end
