# Error model definition
class Error
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  # Define the set of attributes that can be accessed
  attr_accessor :text, :status, :url, :method

  def _links
    { url: request, entry: entry }
  end

  # This method serializes the request object
  def request
    href = URI.encode(url)
    { href: href, method: method, rel: 'request'}
  end

  # This method specifies a default entry point to
  # return to the client
  def entry
    href = URI.encode('api/v1')
    { href: href, method: 'GET', rel: 'entry point',
      params: params }
  end

  # This is how we display the params submitted through
  # the query
  def params
    { point: { value: '{lon},{lat}', optional: true },
      title: { value: 'text', optional: true } }
  end

  # The following method serializes the error object
  def serializer
    { error: { url: url, text: text, status: status,
               method: method, links: _links } }
  end

  # Initializing error object and its attributes
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name} = ", value)
    end
  end

  # Define the error object as not persisted
  # therefore not saving it to database
  def persisted?
    false
  end
end
