# Describes a city and it's geographical information
class Location < ApplicationRecord
  include GeoFactory
end
